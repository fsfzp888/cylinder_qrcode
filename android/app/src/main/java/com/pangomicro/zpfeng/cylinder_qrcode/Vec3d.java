package com.pangomicro.zpfeng.cylinder_qrcode;

/**
 * Created by zczx1 on 2016/4/11.
 */
public class Vec3d {
    public Vec3d(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    private double a;
    private double b;
    private double c;
    public double getA() {
        return a;
    }
    public void setA(double a) {
        this.a = a;
    }
    public double getB() {
        return b;
    }
    public void setB(double b) {
        this.b = b;
    }
    public double getC() {
        return c;
    }
    public void setC(double c) {
        this.c = c;
    }


}
